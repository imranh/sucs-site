<?php
/* mechanism for members to give us feedback about web pages */

// where do website feedback mails go?
$contact = "staff@sucs.org";

if ($session->loggedin) {
    if ($_REQUEST["action"] == "feedback") {
        $feedback = $_REQUEST["feedback"];

        $msgbody = "{$session->username} had this to say about the page {$_SERVER['REQUEST_URI']}:\r\n\r\n";
        $msgbody .= "\"{$feedback}\"\r\n";

        mail($contact, "SUCS Website Feedback", $msgbody);
        $smarty->assign("feedbacked", TRUE);
    }
    $smarty->fetch("feedback.tpl");
}


?>

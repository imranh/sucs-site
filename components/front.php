<?php

$smarty->assign('extra_styles', "css/front.css");

if (!$session->loggedin) {
	$output2 = file_get_contents("../static/fragments/Join.txt");
} else {
	$smarty->assign("news",$DB->GetArray("(SELECT * FROM news ORDER BY date DESC LIMIT 1) UNION SELECT * FROM news ORDER BY date DESC LIMIT 4"));
	$output2 = "<div class=\"panel\"><h3>You are logged in</h3><p style=\"text-align: center;\">Why not join our <a href=\"#\" onclick=\"window.open('/mw/','Milliways','height=600,width=800,menubar=no,resizable=yes,location=no,directories=no,scrollbars=yes,status=no,toolbar=no')\" style=\"font-size: 150%; font-weight: bold; color: #ffc62b;\">Live Chat</a>?</p><p>If you would like to contribute to the site or report a bug, please contact imranh.</p></div>";
	include('users.php');
	//$output .= $result;
	include('electionreminder.php');
	$output2 .= $result;
}

$output = $smarty->fetch("front.tpl");
$smarty->assign("title", "Home");
$smarty->assign("body", $output);

$smarty->assign("secondary",$output2);

?>

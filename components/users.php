<?php
// Generate the list of who's logged in
$users = $DB->GetAll("SELECT DISTINCT username FROM session WHERE username IS NOT NULL;");

$smarty->assign('users', $users);

$result = $smarty->fetch('users.tpl');
?>

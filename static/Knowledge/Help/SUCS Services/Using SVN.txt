



<h2>Why do I want a version control system?</h2>
<p>Placing files you often work with under version control has several benefits:</p>
<ul>
	<li><dl>
		<dt>Undo changes</dt>
		<dd>Sometimes when you&#39;re editing that file, you&#39;ll wish you could go back to the version that actually worked. Now you can!</dd>
	</dl></li>
	<li><dl>
	   <dt>Easily carry your work between computers</dt>
	   <dd>If you ever find yourself wondering which computer you left the latest version of X on, version control can help.</dd>
	</dl></li>
	<li><dl>
		<dt>Track the changes made to a file</dt>
		<dd>With Subversion you can easily get an accurate picture of what you&#39;ve changed and when.</dd>
    </dl></li>
    <li><dl>
    	<dt>Allow a group of people to collaborate effectively</dt>
    	<dd>Version control becomes near <em>essential</em> for any project where several people share responsibility for the same group of files.</dd>
    </dl></li>
</ul>

<p><a href="http://subversion.tigris.org">Subversion</a> is a free, widely used version control system which is easy to use with your SUCS account.</p>               



<h2>Creating a repository</h2>
<pre class="console">$ ssh sucs.org # Connect to the main SUCS server<br />$ svnadmin create repos<br /></pre>
<p>This creates a new empty Subversion repository named &quot;repos&quot;</p>
<p>You may now import an existing directory tree into the repository:</p>
<pre class="console">$ svn import {path of stuff to import} {path of repository}<br /></pre>
<h2>Accessing your repository</h2>
<p>Once you have set up a repository, you may access it remotely using a Subversion client. A simple but powerful command-line client exists, in addition to graphical front-ends for most platforms.</p>
    <h3>Installing a Subversion client</h3>
         <dl>
         <dt>Linux</dt>
         <dd>Subversion is included in all major linux distributions, such as Ubuntu, Fedora Core, SuSE and Mandriva. You may use your distribution&#39;s standard package management tools to install it.</dd>
         <dt>Windows</dt>
         <dd>The command-line client is available for Windows, but you will likely prefer to use a graphical client instead. <a href="http://tortoisesvn.tigris.org/">TortoiseSVN</a> is a Windows shell extension, providing access to version control commands through your right-click context menu.</dd>
         <dt>Mac OS X</dt>
         <dd>The easiest way to get SVN for Mac OS X is via <a href="http://fink.sourceforge.net/">fink</a></dd>
         </dl>
      <p>You can visit the <a href="http://subversion.tigris.org/project_packages.html">Subversion packages page</a> for more information.</p>
  <h3>Check out a &quot;working copy&quot;</h3>
  <p>In order to view and make changes to files in a Subversion repository, you must first perform a &quot;checkout&quot; operation. This downloads a copy of the files you selected from the repository to your computer, ready to be edited.</p>
 <p>The following command will check out a working copy of everything in our repository:</p>
 <pre class="console">$ svn checkout svn+ssh://{username}@sucs.org/home/member/{username}/repos</pre>

<h2>Basic Subversion usage</h2>
<p>Below are the commands you are likely to use often, to keep your working copy(s) synchronised with your Subversion repository:</p> 
    <h3>After you make changes to a file</h3>
          <pre class="console">$ svn commit -m &quot;Changes made&quot;</pre>
    <h3>Fetching the latest revision</h3>
          <pre class="console">$ svn update</pre>
    <h3>Placing a new file under version control</h3>
          <pre class="console">$ svn add {file(s)}</pre>
 	<h3>Dealing with conflicts</h3>When you have more than one edit of one line you will have a conflict. When this happens, you have to manualy merge the two changes. You do this in your editor of choice. Once you&#39;ve patched up the file you need to tell Subversion<br /><pre class="console">$ svn resolved {file(s)}<br /></pre>
 
 <p>For more on resolving conflicts see the <a href="http://svnbook.red-bean.com/nightly/en/svn.tour.cycle.html#svn.tour.cycle.resolve">Subversion Book</a><br /></p><h2>Further Reading</h2>
 <p>If you want to know more about using Subversion, you can take a look at the following links:</p>
 <dl>
 <dt><a href="http://www.onlamp.com/pub/a/onlamp/2002/10/31/subversion.html">Single-User Subversion</a></dt>
 <dd>A slightly more comprehensive tutorial on using Subversion in a single-user environment.</dd>
 <dt><a href="http://svnbook.red-bean.com/">Version Control with Subversion</a></dt>
 <dd>A free book about using Subversion, including a large reference section.</dd>
  </dl>

<div class="box">
	<div class="boxhead"><h2>Ian Tucker (bogglesteinsky) - I Can Hear You Typing</h2></div>
	<div class="boxcontent">
	<p>Taking inspiration from the book 'Silence on the Wire', Ian gives a taster of how your communications might not be as secure as you thought they were...</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-05-15/2007-05-15-bogglesteinsky.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-05-15/2007-05-15-bogglesteinsky.flv" image="/videos/talks/2007-05-15/2007-05-15-bogglesteinsky.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-05-15/2007-05-15-bogglesteinsky.flv" />
<param name="image" value="/videos/talks/2007-05-15/2007-05-15-bogglesteinsky.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-05-15/2007-05-15-bogglesteinsky.png" />
</object></div>

<p><strong>Length: </strong>10m 29s</p>
<p><strong>Video:</strong><a href="/videos/talks/2007-05-15/2007-05-15-bogglesteinsky.mov" title="784x576 H.264 .mov - 74.3MB">784x576</a>(H.264 .mov, 74.3MB98.3MB)</p>
<p><strong>Slides:</strong> Coming Soon (PDF, -)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>

<div class="box">
<div class="boxhead">
<h2>Tim Clark (eclipse) - "LaTeX presentations with Beamer"</h2>
</div>
<div class="boxcontent">
<p>A look at how to use LaTeX to make presentations with Beamer.</p>
<div id="player">
<object data="/videos/talks/mediaplayer.swf?file=2008-10-16/eclipse.flv" height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2008-10-16/eclipse.flv" />
<param name="image" value="/videos/talks/2008-10-16/eclipse.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars" value="image=/videos/talks/2008-10-16/eclipse.png" />
</object>
</div>
<p><strong>Length: </strong>8m 7s</p>
<p><strong>Video: </strong><a href="/videos/talks/2008-10-16/eclipse.ogg" title="720x576 Ogg Theora - 52.8MB">720x576</a> (Ogg Theora, 52.8MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>

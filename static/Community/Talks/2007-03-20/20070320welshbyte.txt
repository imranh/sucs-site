<div class="box">
	<div class="boxhead"><h2>Andy Price (welshbyte) - &quot;Why Pybackpack Sucks&quot;</h2></div>
	<div class="boxcontent">
	<p>Andy Price talks about the problems he&#39;s found in Pybackpack since he took over its development, and some of the things he is proposing to improve it.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-03-20/welshbyte.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-03-20/welshbyte.flv" image="/videos/talks/2007-03-20/welshbyte.png" id="player" displayheight="240">
<param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-03-20/welshbyte.flv" />
<param name="image" value="/videos/talks/2007-03-20/welshbyte.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-03-20/welshbyte.png" />
</object></div>

<p><strong>Length: </strong>13m 57s</p><p><strong>Video: </strong><a href="/videos/talks/2007-03-20/2007-03-20-welshbyte.ogg" title="784x576 Ogg Theora - 72.5MB">784x576</a> (Ogg Theora, 72.5MB)<br /><strong>Slides:</strong> <a href="/videos/talks/2007-03-20/2007-03-20-welshbyte-slides.pdf" title="Why Pybackpack Sucks slides">2007-03-20-welshbyte-slides.pdf</a> (PDF, 636KB)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>

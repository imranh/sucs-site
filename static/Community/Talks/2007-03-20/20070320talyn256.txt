<div class="box">
	<div class="boxhead"><h2>Sean Handley (talyn256) - &quot;The Wonderful World of NXT&quot;</h2></div>
	<div class="boxcontent">
	<p>Sean Handley shows a handful of the endless possibilities available with the new Lego Mindstorms NXT.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-03-20/talyn256.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-03-20/welshbyte.flv" image="/videos/talks/2007-03-20/welshbyte_preview.png" id="player" displayheight="240">
<param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-03-20/talyn256.flv" />
<param name="image" value="/videos/talks/2007-03-20/talyn256_preview.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-03-20/talyn256_preview.png" />
</object></div>

<p><strong>Length: </strong>11m 26s</p><p><strong>Video: </strong><a href="/videos/talks/2007-03-20/2007-03-20-talyn256.ogg" title="784x576 Ogg Theora - 135.4MB">784x576</a> (Ogg Theora, 135.4MB)<br /><strong>Slides:</strong> <a href="/videos/talks/2007-03-20/2007-03-20-talyn256-slides.pdf" title="Mindstorms NXT slides">2007-03-20-talyn256-slides.pdf</a> (PDF, 2.3MB)<br /></p></div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>

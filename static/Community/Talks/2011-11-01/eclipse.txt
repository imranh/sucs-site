<div class="box">
<div class="boxhead">
<h2>Tim Clark (eclipse) - "How medium sized mammals defend our network!"</h2>
</div>
<div class="boxcontent">
<div id="player">
<object data="/videos/talks/mediaplayer.swf?file=2011-11-01/eclipse.flv" height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2011-11-01/eclipse.flv" />
<param name="image" value="/videos/talks/2011-11-01/eclipse.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars" value="image=/videos/talks/2011-11-01/eclipse.png" />
</object>
</div>
<p><strong>Length: </strong>19m 0s</p>
<p><strong>Video: </strong><a href="/videos/talks/2011-11-01/eclipse.ogv" title="720x576 Ogg
Theora - 30MB">720x576</a> (Ogg Theora, 30MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>

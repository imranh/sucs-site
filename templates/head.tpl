<!doctype html>
<html lang="{$language.code}">

<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <title>{$title} - SUCS</title>
    <link rel="SHORTCUT ICON" href="{$baseurl}/favicon.ico"/>
    <link rel="apple-touch-icon" href="{$baseurl}/images/apple-touch-icon.png"/>

	<link rel="stylesheet" type="text/css" href="{$baseurl}/css/main.css" media="screen" />
{if isset($extra_styles)}
	{foreach from=$extra_styles item=style}
		<link rel="stylesheet" type="text/css" href="{$baseurl}/{$style}" media="screen" />
	{/foreach}
{/if}
	<link rel="stylesheet" href="{$baseurl}/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/common.css" media="screen,print"/>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/sucs.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/box.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/downloads.css" media="screen"/>
    {if isset($extra_styles)}
        {foreach from=$extra_styles item=style}
            <link rel="stylesheet" type="text/css" href="{$style}" media="screen"/>
        {/foreach}
    {/if}
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/print.css" media="print"/>

    {if isset($rss_url)}
        <link rel="alternate" type="application/rss+xml" title="{$title}" href="{$rss_url}"/>
    {/if}
    {if isset($atom_url)}
        <link rel="alternate" type="application/atom+xml" title="{$title}" href="{$atom_url}"/>
    {/if}
    <meta name="description"
          content="Swansea University Computer Society - providing student computing facilities and personal web pages."/>
    {if $refresh}
        <meta http-equiv="REFRESH" content="{$refresh}" />{/if}

    {if isset($extra_scripts)}
        {foreach from=$extra_scripts item=script}
            {$script}
        {/foreach}
    {/if}

</head>
<body>

{include file="branding.tpl"}

<div id="bodyer">

{include file="usermessages.tpl"}

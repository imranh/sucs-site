<div id="breadcrumb">
    {if $pathlist}
        <ul>
            {foreach name=pathlist from=$pathlist key=key item=item}
                <li>{if !$smarty.foreach.pathlist.first}{if $item!=""}&gt; {/if}{/if}{if !$smarty.foreach.pathlist.last}
                    <a href="{$baseurl}{buildpath list=$pathlist item=$item}">{/if}{if $item}{$item|escape:'htmlall'}{else}{if $smarty.foreach.pathlist.first}SUCS{/if}{/if}{if !$smarty.foreach.pathlist.last}</a>{/if}
                </li>
            {/foreach}
        </ul>
    {/if}
</div>

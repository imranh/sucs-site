<div id="menu">
	<ul id="menulist">
		{foreach name=menu from=$menu key=name item=target}
			{if strpos($path, $name)}
				<li id="menu-active">
					<a href="{$baseurl}/{$name}">
						{$name}
					</a>
			{else}
				<li>
					<a href="{$baseurl}{$target}">
						{$name}
					</a>
				</li>
			{/if}
		{/foreach}
</div>
<div id="submenu">
	<ul id="submenulist">
			{foreach name=menu from=$submenu key=name item=target}
				{if $target==$path}
					<li id="submenu-active">
						<a href="{$baseurl}{$target}">{$name}</a>
					</li>
				{else}
					<li>
						<a href="{$baseurl}{$target}">{$name}</a>
					</li>
				{/if}
			{/foreach}
	</ul>
</div>

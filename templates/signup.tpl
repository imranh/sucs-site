{if $mode=='login'}
    <form action="{$componentpath}" method="post">
        <div class="box" style="width: 70%; margin: auto;">
            <div class="boxhead"><h2>Membership Signup</h2></div>
            <div class="boxcontent">

                <p>Please enter the details from your signup receipt</p>

                <div class="row">
                    <label for="signupid">Signup ID:</label>
                    <span class="textinput"><input type="text" size="20" name="signupid" id="signupid"/></span>
                </div>
                <div class="row">
                    <label for="signuppw">Password:</label>
                    <span class="textinput"><input type="text" size="20" name="signuppw" id="signuppw"/></span>
                </div>
                <div class="row"><span class="textinput">
                <input type="submit" name="submit" value="Sign Up"/></span>
                </div>
                <div class="clear"></div>
                <div class="note">If you wish to renew an existing account instead, please login to <a
                            href="{$baseurl}/Options">Membership Options</a> using your existing account details.
                </div>
            </div>
            <div class="hollowfoot">
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    </form>
{elseif $mode=='form' || $mode=="re-form"}
    <h1>Signup</h1>
    <form id="mainform" action="{$componentpath}" method="post">
        {if $usertype==1}
            <div class="row" id="studentiddiv">
                <label for="studentid">Student Number</label>
                <span class="textinput"><input type="text" id="studentid" name="studentid" size="30"
                                               {if $mode=='re-form'}value='{$fields.studentid}'{/if} /></span>

                <div id="studentidmessage"{if $mode=='re-form'}{if isset($errors.studentid)}
                     style="color:red; float:right; clear:right;">{$errors.studentid}{else} style="color:green;
                    float:right; clear:right;">OK{/if}{else}>{/if}</div>
            </div>
        {/if}
        <div class="row" id="usernamediv">
            <label for="username">Username</label>
            <span class="textinput"><input type="text" id="username" name="username" size="30"
                                           {if $mode=='re-form'}value='{$fields.username}'{/if}/></span>

            <div id="usernamemessage"{if $mode=='re-form'}{if isset($errors.username)}
                 style="color:red; float:right; clear:right;">{$errors.username}{else} style="color:green; float:right;
                clear:right;">OK{/if}{else}>{/if}</div>
        </div>
        <div class="row" id="realnamediv">
            <label for="realname">{if $usertype!=2}Real Name{else}Society Name{/if}</label>
            <span class="textinput"><input type="text" id="realname" name="realname" size="30"
                                           {if $mode=='re-form'}value='{$fields.realname}'{/if}/></span>

            <div id="realnamemessage"{if $mode=='re-form'}{if isset($errors.realname)}
                 style="color:red; float:right; clear:right;">{$errors.realname}{else} style="color:green; float:right;
                clear:right;">OK{/if}{else}>{/if}</div>
        </div>
        {if $usertype!=2}
            <div class="row" id="postcodediv" style="display:none">
                <label for="postcode">Post Code</label>
                <span class="textinput"><input type="text" id="postcode" size="10" value=""/></span>

                <div id="postcodemessage"></div>
            </div>
            <div class="row" id="addseldiv" style="display:none">
                <label for="addsel">Address Selector</label>
                <span class="textinput"><select id="addsel" value=""></select></span>
            </div>
            <div class="row" id="addressdiv">
                <label for="address">{if $usertype==1}Term Time {/if}Address</label>
                <span class="textinput"><textarea id="address" name="address" cols="35"
                                                  rows="4">{if $mode=='re-form'}{$fields.address}{/if}</textarea></span>

                <div id="addressmessage"{if $mode=='re-form'}{if isset($errors.address)}
                     style="color:red; float:right; clear:right;">{$errors.address}{else} style="color:green;
                    float:right; clear:right;">OK{/if}{else}>{/if}</div>
            </div>
        {else}
            <div class="row" id="contactdiv">
                <label for="contact">Contact Name</label>
                <span class="textinput"><input type="text" id="contact" name="contact" size="30"
                                               {if $mode=='re-form'}value='{$fields.contact}'{/if}/></span>

                <div id="contactmessage"{if $mode=='re-form'}{if isset($errors.contact)}
                     style="color:red; float:right; clear:right;">{$errors.contact}{else} style="color:green;
                    float:right; clear:right;">OK{/if}{else}>{/if}</div>
            </div>
        {/if}
        <div class="row" id="emaildiv">
            <label for="email">Email Address</label>
            <span class="textinput"><input type="text" id="email" name="email" size="30"
                                           {if $mode=='re-form'}value='{$fields.email}'{/if}/></span>

            <div id="emailmessage"{if $mode=='re-form'}{if isset($errors.email)}
                 style="color:red; float:right; clear:right;">{$errors.email}{else} style="color:green; float:right;
                clear:right;">OK{/if}{else}>{/if}</div>
        </div>
        <div class="row" id="phonediv">
            <label for="phone">Phone Number</label>
            <span class="textinput"><input type="text" id="phone" name="phone" size="30"
                                           {if $mode=='re-form'}value='{$fields.phone}'{/if}/></span>

            <div id="phonemessage"{if $mode=='re-form'}{if isset($errors.phone)}
                 style="color:red; float:right; clear:right;">{$errors.phone}{else} style="color:green; float:right;
                clear:right;">OK{/if}{else}>{/if}</div>
        </div>
        <div class="row" id="tncdiv">
            <span class="textinput"><input type="checkbox" id="tnc" name="tnc" size="30"
                                           {if $mode=='re-form'}value='{$fields.tnc}'{/if}/>I have read and understood, and agree to be bound by the <a
                        href="{$baseurl}/About/Conditions">Terms and Conditions</a>.</span>

            <div id="tncmessage"{if $mode=='re-form'}{if isset($errors.tnc)}
                 style="color:red; float:right; clear:right;">{$errors.tnc}{else} style="color:green; float:right;
                clear:right;">OK{/if}{else}>{/if}</div>
        </div>
        {if $overridable}
            <div class="row" id="overridediv">
                <label for="overridable">Override Validation</label>
                <span class="textinput"><input type="checkbox" id="override" name="override"/></span>
            </div>
        {/if}
        <input type="hidden" id="signupid" name="signupid" value="{$signupid}"/>
        <input type="hidden" id="signuppw" name="signuppw" value="{$signuppw}"/>

        <div class="row" id="submitdiv">
            <input type="submit" id="submit" value="Submit"/>
        </div>
    </form>
{elseif $mode=='result'}
    {if !$failed}
        <h1>Welcome to SUCS</h1>
        <p>Signup is complete, please see below for your password, a copy has also been send to {$email}, we request you
            change this immediatley. See our <a href="{$baseurl}/Getting%20Started">Getting Started</a> page for some
            ways you can start using your new SUCS account!</p>
        <p>
            Username: <strong>{$username}</strong><br/>
            Password: <strong>{$password}</strong><br/>
        </p>
    {else}
        <h1>Error</h1>
        An error occured during signup, please email
        <a href='mailto:admin@sucs.org'>admin@sucs.org</a>
        for assistance.
    {/if}
{/if}

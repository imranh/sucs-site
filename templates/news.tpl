{if $editable}
	<div id="rightside" class="edit">
	<ul>
		<li><a href="?action=create">Create</a></li>
	</ul>
	</div>
{/if}

<div id="news">
{foreach name=news from=$news key=itemnum item=item}
<div class="panel">
	<div class="boxhead"><h2><a id="{$item.id}">{$item.title}</a></h2></div>
	<div class="boxcontent">
	{$item.body}

{if $editable}
<div class="edit">
<ul>
<li><a href="{$item.title|escape:'url'}?action=delete-query">Delete</a></li>
<li><a href="{$item.title|escape:'url'}?action=edit">Edit</a></li>
{if $item.expirytime > $smarty.now}
<li><a href="{$item.title|escape:'url'}?action=expire-query">Expire</a></li>
{/if}
</ul>
<div class="clear"></div>
</div>
{/if}
	</div>
	<div class="boxfoot"><p>Posted by <a href="{$baseurl}/Community/Members/{$item.author}">{$item.author}</a> on {$item.date|date_format }</p></div>
</div>
{foreachelse}
<p>News item not found</p>
{/foreach}
</div>

<div id="user-area">
	<form method="post" action="{$ssl_url|escape:'htmlall'}{getparams gets=$gets}" autocomplete="off">
		{if $session->loggedin}
			<div id="logged-in">
				<a id="user-aera-icon" href="{$baseurl}/Community/Members/{$session->username}"><img src="/pictures/people/{$session->username}.png" height="56"></a> 
					{if $session->email_forward}
						mail forwarded to {$session->email_forward}<br />
					{else}
					{if $session->email==0}
						<a href="https://sucs.org/webmail">No new mail</a>
					{elseif $session->email==1}
						<a href="https://sucs.org/webmail">New mail</a>
					{/if}
				<a href="{$baseurl}/Options">Account Options</a>
				<a>Print balance: {$session->printbalance}</a>
				<button type="submit" name="Logout" id="Logout" value="Logout">Logout</button>
		{/if}
		{else}
			<div id="login">
				<div id="login-form">
					<div id="username-feild">
						<span class="fa fa-user"></span>
						<input type="text" class="text" name="session_user" id="session_user" placeholder="Username" autocomplete="off" />
					</div>
					<div id="password-feild">
						<span class="fa fa-key"></span>
						<input type="password" class="text" name="session_pass" id="session_pass" placeholder="Password" autocomplete="off" />
					</div>
					<input type="hidden" name="token" value="{$session->token}" />
				</div>
				<div id="login-button">
					<button type="submit" class="button" name="Login" title="Login" id="login-button" value="Login" />
						<span class="fa fa-sign-in fa-2x">
							<span class="login-button-text"> Login</span>
						</span>
					</button>
				</div>
		{/if}
			</div>
	</form>
</div>

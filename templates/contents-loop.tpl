{math equation="x+1" x=$level assign=level}
{if $level > 0}
<ul>
    {/if}
    {foreach name=contents from=$data key=name item=item}
        {if $level == 0}<h3>{else}<li>{/if}<a href="{$rootnode}{$parent}/{$name}">{$name}</a>{if $level == 0}</h3>
    {/if}{if $item.summary}{if $level > 0} - {/if}{$item.summary}{/if}{if is_array($item.file)}
            {include file="contents-loop.tpl" data=$item.file parent="$parent/$name" level=$level}
    {/if}
    {if $level > 0}</li>{/if}
    {/foreach}
    {if $level > 0}
</ul>
{/if}


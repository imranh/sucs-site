{include file="../static/fragments/Front.txt"}
<div id="bottomleftside">
{if $session->loggedin}
	{foreach name=news from=$news key=itemnum item=item}
		<div id="bottomleftsidebox" class="panel">
			<h3>{$item.title}</h3>
			<div>
				{articlesummary id=$item.id article=$item.body title=$item.title}
			</div>
		</div>
	{/foreach}
{else}

	<div id="bottomleftsidebox" class="panel">
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id lacus fringilla, ultricies quam vitae, convallis erat. Nulla porttitor, odio a cursus pulvinar, justo quam interdum lectus, sit amet hendrerit felis nunc vitae tortor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce augue orci, lobortis eget lectus vitae, venenatis vulputate ex. Curabitur ullamcorper ultricies justo, vel ullamcorper tellus consequat eget. In dictum congue ullamcorper. Maecenas vitae consequat diam, nec suscipit nunc. Duis nec massa id mauris fringilla efficitur eget vel ex.
		</p>
	</div>
	
	<div id="spaceor">
	</div>

	<div id="bottomleftsidebox" class="panel">
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id lacus fringilla, ultricies quam vitae, convallis erat. Nulla porttitor, odio a cursus pulvinar, justo quam interdum lectus, sit amet hendrerit felis nunc vitae tortor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce augue orci, lobortis eget lectus vitae, venenatis vulputate ex. Curabitur ullamcorper ultricies justo, vel ullamcorper tellus consequat eget. In dictum congue ullamcorper. Maecenas vitae consequat diam, nec suscipit nunc. Duis nec massa id mauris fringilla efficitur eget vel ex.
		</p>
	</div>

	<div id="spaceor">
	</div>

	<div id="bottomleftsidebox" class="panel">
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id lacus fringilla, ultricies quam vitae, convallis erat. Nulla porttitor, odio a cursus pulvinar, justo quam interdum lectus, sit amet hendrerit felis nunc vitae tortor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce augue orci, lobortis eget lectus vitae, venenatis vulputate ex. Curabitur ullamcorper ultricies justo, vel ullamcorper tellus consequat eget. In dictum congue ullamcorper. Maecenas vitae consequat diam, nec suscipit nunc. Duis nec massa id mauris fringilla efficitur eget vel ex.
		</p>
	</div>
{/if}
</div>
</div>
